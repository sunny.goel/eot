package com.eot.entity;

import com.eot.utils.InputReader;

public class ConsolePlayer implements Player {
    private int score;
    private InputReader inputReader;

    public ConsolePlayer(InputReader inputReader) {
        this.inputReader = inputReader;
    }

    public int getScore() {
        return this.score;
    }

    public void updateScore(int newScore) {
        this.score += newScore;
    }

    public Move getMove() {
        System.out.println("Player: Enter your choice");
        String move = inputReader.readLine();

        if (move.toLowerCase().equals("ch")) {
            return Move.CHEAT;
        }
        if (move.toLowerCase().equals("co")) {
            return Move.COOPERATE;
        }

        return null;
    }
}
