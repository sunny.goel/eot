package com.eot.entity;

public interface Player {

    public Move getMove();

    public void updateScore(int score);

    public int getScore();

}
