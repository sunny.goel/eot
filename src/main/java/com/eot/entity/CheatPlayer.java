package com.eot.entity;

public class CheatPlayer implements Player {
    private int score;


    @Override
    public Move getMove() {
        return Move.CHEAT;
    }

    @Override
    public void updateScore(int score) {
        this.score += score;
    }

    @Override
    public int getScore() {
        return this.score;
    }
}
