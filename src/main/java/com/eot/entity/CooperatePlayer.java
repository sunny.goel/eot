package com.eot.entity;

public class CooperatePlayer implements Player {
    private int score;

    @Override
    public Move getMove() {
        return Move.COOPERATE;
    }

    @Override
    public void updateScore(int score) {
    this.score += score;
    }

    @Override
    public int getScore() {
        return score;
    }
}
