package com.eot.entity;


public class Machine {

    public ScoreCard getScores(Move p1, Move p2) {

        if(p1.equals(Move.CHEAT) && p2.equals(Move.CHEAT)) {
            return new ScoreCard(0,0);
        }

        if(p1.equals(Move.COOPERATE) && p2.equals(Move.COOPERATE)) {
            return new ScoreCard(2,2);
        }

        if(p1.equals(Move.CHEAT) && p2.equals(Move.COOPERATE)) {
            return new ScoreCard(3,-1);
        }

        if(p1.equals(Move.COOPERATE) && p2.equals(Move.CHEAT)) {
            return new ScoreCard(-1,3);
        }
        return null;

    }
}
