package com.eot;

import com.eot.entity.Game;
import com.eot.entity.ConsolePlayer;
import com.eot.utils.InputReader;
import com.eot.utils.InputReaderImpl;

public class EotApplication {

    public static void main(String args[]) {
        InputReader inputReader = new InputReaderImpl();
        int rounds =5;
        Game game = new Game(rounds, new ConsolePlayer(inputReader), new ConsolePlayer(inputReader));
        game.start();
    }
}
