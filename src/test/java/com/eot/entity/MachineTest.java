package com.eot.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTest {

    private Machine machine;

    @Before
    public void init() {
        machine = new Machine();
    }

    @Test
    public void should_return_score_pair() {
        ScoreCard scoreCard = machine.getScores(Move.CHEAT, Move.CHEAT);
        Assert.assertNotNull(scoreCard);
    }

    @Test
    public void should_return_0_0_for_both_cheat() {
        ScoreCard scoreCard = machine.getScores(Move.CHEAT, Move.CHEAT);
        Assert.assertEquals(0, scoreCard.getScore1());
        Assert.assertEquals(0, scoreCard.getScore2());
    }

    @Test
    public void should_return_2_2_for_both_cooperate() {
        ScoreCard scoreCard = machine.getScores(Move.COOPERATE, Move.COOPERATE);
        Assert.assertEquals(2, scoreCard.getScore1());
        Assert.assertEquals(2, scoreCard.getScore2());
    }

    @Test
    public void should_return_3_n1_for_cheat_cooperate() {
        ScoreCard scoreCard = machine.getScores(Move.CHEAT, Move.COOPERATE);
        Assert.assertEquals(3, scoreCard.getScore1());
        Assert.assertEquals(-1, scoreCard.getScore2());
    }

    @Test
    public void should_return_n1_3_for_cooperate_cheat() {
        ScoreCard scoreCard = machine.getScores(Move.COOPERATE, Move.CHEAT);
        Assert.assertEquals(-1, scoreCard.getScore1());
        Assert.assertEquals(3, scoreCard.getScore2());
    }
}
